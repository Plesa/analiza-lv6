﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        double op1, op2, r;
        public Form1()
        {
            InitializeComponent();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            if (!double.TryParse(textBox2.Text, out op2)) MessageBox.Show("Krivi unos operanda!");
            r = op1 + op2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            if (!double.TryParse(textBox2.Text, out op2)) MessageBox.Show("Krivi unos operanda!");
            r = op1 - op2;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            if (!double.TryParse(textBox2.Text, out op2)) MessageBox.Show("Krivi unos operanda!");
            r = op1 * op2;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            if (!double.TryParse(textBox2.Text, out op2)) MessageBox.Show("Krivi unos operanda!");
            r = op1 / op2;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            r = Math.Sin(op1);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            r = Math.Cos(op1);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            r = Math.Log(op1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            r = Math.Pow(op1,2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1)) MessageBox.Show("Krivi unos operanda!");
            r = Math.Sqrt(op1);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            labREZ.Text = r.ToString();
            
        }
    }
}
