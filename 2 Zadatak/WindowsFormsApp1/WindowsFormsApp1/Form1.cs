﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        int bp;
        string ril, rul;

        public void Rand()
        {

            Random rand = new Random();
            ril = list[rand.Next(0, list.Count - 1)];
            bp = ril.Length + 3;
            label1.Text = bp.ToString();
            lab_slova.Text = ril.Length.ToString(); 
            rul = new string('?', ril.Length);
           
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tb.Text.Length == 1)
            {
                if (ril.Contains(tb.Text))
                {
                    string tempr = ril;

                    while (tempr.Contains(tb.Text))
                    {
                        int i = tempr.IndexOf(tb.Text);

                        StringBuilder builder = new StringBuilder(tempr);
                        builder[i] = '?';
                        tempr = builder.ToString();

                        StringBuilder builder2 = new StringBuilder(rul);
                        builder2[i] = Convert.ToChar(tb.Text);
                        rul = builder2.ToString();

                        MessageBox.Show("Pogodili ste " + (i + 1) + ". " + "slovo");


                    }


                    if (rul == ril)
                    {
                        MessageBox.Show("Čestitam pobijedili ste!");

                    }
                    else
                    {
                        bp--;
                        label1.Text = bp.ToString();
                        if (bp == 0)
                        {
                            MessageBox.Show("Izgubili ste!");

                        }
                    }
                }
            }
            else if (tb.Text.Length > 1)
            {
                if (ril == tb.Text)
                {
                    MessageBox.Show("Čestitam pobijedili ste!");

                }
            }
            else
            {
                bp--;
                label1.Text = bp.ToString();
                if (bp == 0)
                {
                    MessageBox.Show("Izgubili ste!");

                }
            }
        }

        private void btn_Q_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string rijec;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"C:\Users\Plesa\source\repos\WindowsFormsApp1\WindowsFormsApp1\TextFile1.txt"))
            {
                while ((rijec = reader.ReadLine()) != null)
                {
                    list.Add(rijec);
                }
                Rand();
            }
        }
    }
}
